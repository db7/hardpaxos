HardPaxos
=========

State Machine Replication (SMR) is a common technique to make services fault-tolerant.
Practical SMR systems tolerate process crashes, but no hardware errors such as bit flips.  
Still, hardware errors can cause major service outages, and their rate is expected to increase in the future.
Current approaches either incur a high overhead by hardening large parts of the system in software, or increase the cost of ownership by introducing additional hardware components.

HardPaxos is an atomic broadcast algorithm for SMR that enables services to tolerate hardware errors.
HardPaxos requires no additional hardware and has only a small part of its functionality hardened using, for example, AN-encoding.
Consequently, HardPaxos incurs little performance and state overhead.

This repository contains the description of the complete `HardPaxos algorithm <https://bitbucket.org/db7/hardpaxos/src/tip/techrep.pdf>`_.
The source code of HardPaxos is not yet open source, but can be made accessible upon request.

Software components
-------------------

- `BFI fault injector <https://bitbucket.org/db7/bfi>`_
- `ZTAS communication framework <https://bitbucket.org/db7/ztas>`_
- `HardPaxos Replicated State Machine library and examples <https://bitbucket.org/db7/rsm>`_